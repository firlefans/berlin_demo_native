//
//  N4Favourite.m
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import "N4Favourite.h"


@implementation N4Favourite

@dynamic url;
@dynamic comment;
@dynamic title;
@dynamic previewUrl;

@end
