//
//  N4FavouriteListViewController.h
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueueHelper.h"
#import "N4Favourite.h"
#import "N4FlickrImageCell.h"

@interface N4FavouriteListViewController : UITableViewController

@end
