//
//  N4AddFavouriteViewController.m
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import "N4AddFavouriteViewController.h"

@interface N4AddFavouriteViewController ()

@end

@implementation N4AddFavouriteViewController {
    N4FlickrImage *_image;
    UITextField *_tf;
}

- (id)initWithFlickrImage:(N4FlickrImage*)image
{
    _image = image;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame = CGRectMake(0.0f, 20.0f, 320.0f, 460.0f);
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Favourite";

    //self.navigationItem.rightBarButtonItem = saveBtn;
	// Do any additional setup after loading the view.
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 50.0f, 290.0f, 40.0f)];
    label.text = @"Add as favourite";
    
    _tf = [[UITextField alloc] initWithFrame:CGRectMake(20.0f, 85.0f, 290.0f, 40.0f)];
    [_tf becomeFirstResponder];
    
    //tf.backgroundColor = [UIColor grayColor];
    _tf.placeholder = @"Comment";
    _tf.borderStyle = UITextBorderStyleRoundedRect;
    
    UIButton *saveBtn = [[UIButton alloc] initWithFrame: CGRectMake(20.0f, 125.0f, 290.0f, 35.0f) ];
    saveBtn.backgroundColor = UIColor.grayColor;
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveAction) forControlEvents: UIControlEventTouchUpInside];
    
    [self.view addSubview:label];
    [self.view addSubview:_tf];
    [self.view addSubview:saveBtn];
}

- (void)saveAction
{
    N4Favourite *fav = [N4Favourite create];
    fav.url = _image.url;
    fav.previewUrl = _image.previewURL;
    fav.title = _image.title;
    fav.comment = _tf.text;
    [fav save];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
