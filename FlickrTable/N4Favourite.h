//
//  N4Favourite.h
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface N4Favourite : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * previewUrl;

@end
