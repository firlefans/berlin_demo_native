//
//  N4FlickerImageSource.m
//  FlickrTable
//
//  Created by Diligent Worker on 22.04.13.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//
#import "QueueHelper.h"
#import "N4FlickerImageSource.h"
#import "N4FlickrConstants.h"
#import "N4FlickrImage.h"

@implementation N4FlickerImageSource
{
    NSArray *_images;
    NSOperationQueue *_fetchQueue;
}

- (id)init
{
    _fetchQueue = [NSOperationQueue new];
    _fetchQueue.maxConcurrentOperationCount = 1; //no point in fetching concurrently for the whole list
    return self;
}

- (void)fetchRecentImagesWithCompletion:(void (^)(void))completion
{
    NSString *urlString = [NSString stringWithFormat:@"http://api.flickr.com/services/rest?method=flickr.photos.getRecent&api_key=%@&format=json&nojsoncallback=1", FLICKR_KEY];
    dispatch_block_t fetchBlock = ^
    {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];

        NSURLResponse *response;
        //NSData instead of NSArray
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];

        NSMutableDictionary * jsonData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];

        NSArray *jsonImages = jsonData[@"photos"][@"photo"];
        NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity: jsonImages.count];

        NSMutableString *imageURL;
        NSMutableString *previewURL;

        for( NSDictionary * image in jsonImages )
        {
            NSString *url = [NSString stringWithFormat:@"http://farm%@.staticflickr.com/%@/%@_%@", image[@"farm"], image[@"server"], image[@"id"], image[@"secret"] ];

            imageURL = [[NSMutableString alloc] initWithString:url];
            [imageURL appendString:@"_b.jpg"];
            previewURL = [[NSMutableString alloc] initWithString:url];
            [previewURL appendString:@"_q.jpg"];

            N4FlickrImage * flickerImage = [[N4FlickrImage alloc] initWithTitle:image[@"title"] url:imageURL previewURL:previewURL];
            [images addObject:flickerImage];
        }

        _images = images;
        
        [QueueHelper execSync:completion];

    };
    [_fetchQueue addOperationWithBlock:fetchBlock];

}

- (NSUInteger)count
{
    return _images.count;
}

- (N4FlickrImage*)imageAtIndex:(NSUInteger)index
{
    if( index < _images.count ) {
        return _images[index];
    }
    
    [NSException raise:@"Invalid image Index" format:@"Index %d not found", index];
    
}

@end
