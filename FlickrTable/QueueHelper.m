//
//  QueueHelper.m
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import "QueueHelper.h"

@implementation QueueHelper

+ (void) execSync:(void (^)(void))completionBlock
{
    if(![NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), ^ {
            completionBlock();
        });
    } else {
        completionBlock();
    }
}

+ (void) execAsync:(void (^)(void))completionBlock
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^ {
        completionBlock();
    });
}
@end
