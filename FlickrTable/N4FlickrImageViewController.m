//
//  N4FlickrImageViewController.m
//  FlickrTable
//
//  Created by Diligent Worker on 22.04.13.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//
#import "QueueHelper.h"
#import "N4FlickrImageViewController.h"
#import "N4FlickrImage.h"
#import "N4AddFavouriteViewController.h"
@implementation N4FlickrImageViewController
{
    N4FlickrImage *_image;
    UIImageView *_imageView;
    UIScrollView *_scrollView;
}

#pragma mark - Initialization & Deallocation

- (id)initWithFlickrImage:(N4FlickrImage*)image
{
	if ((self = [super init])) {
        _image = image;
        self.view.backgroundColor = UIColor.whiteColor;
        self.navigationItem.title = _image.title;
	}
    
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _scrollView = [self createScrollView];
    self.navigationItem.rightBarButtonItem = [self createFavouriteButton];
    
    //add the image view before requesting the image
    _imageView = [self createImageView];
    //add image view to scroll view
    [_scrollView addSubview:_imageView];
    //and add the scroll view as a subview of the main view
    [self.view addSubview:_scrollView];

    [_imageView setImageWithURL:[NSURL URLWithString: _image.previewURL]
               placeholderImage:[UIImage imageNamed:@"spinnger.gif"]
                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                          //[cell setPreviewImage:image];
                      }];    
}

- (UIBarButtonItem *) createFavouriteButton
{

    UIBarButtonItem *favouriteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAdd target: self
                                                                          action:@selector(favouriteAction)];

    return favouriteButton;
}
- (UIImageView *) createImageView
{
    _imageView = [[UIImageView alloc] initWithFrame:_scrollView.bounds];
    _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    return _imageView;
}

- (UIScrollView *) createScrollView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [_scrollView setContentSize: CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 10)];
    [_scrollView setScrollEnabled:true];
    _scrollView.minimumZoomScale=1.0;
    _scrollView.maximumZoomScale=3.0;
    _scrollView.delegate=self;
    return _scrollView;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

#pragma TODO - Implement a modal form with a UITextField, and store in SQLite ;)
- (void) favouriteAction
{
    
    NSLog(@"TODO - favourited %@", _image.url);
    N4AddFavouriteViewController *vc = [[N4AddFavouriteViewController alloc] initWithFlickrImage:_image];
    vc.modalPresentationStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:vc animated:true completion:nil];
}

@end