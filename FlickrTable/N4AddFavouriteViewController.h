//
//  N4AddFavouriteViewController.h
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "N4FlickrImage.h"
#import "N4Favourite.h"

@interface N4AddFavouriteViewController : UIViewController
- (id)initWithFlickrImage:(N4FlickrImage*)image;
@end
