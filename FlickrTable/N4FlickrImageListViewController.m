//
//  N4FlickrImageListViewController.m
//  FlickrTable
//
//  Created by Diligent Worker on 22.04.13.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import "N4FlickrImageListViewController.h"

#import "N4FlickrConstants.h"
#import "N4FlickerImageSource.h"
#import "N4FlickrImageCell.h"
#import "N4FlickrImageViewController.h"
#import "N4FlickrImage.h"


@interface N4FlickerImageCacheInfo : NSData
@property (nonatomic,copy) NSString *url;
@property (nonatomic,copy) UIImage *image;
@end

@implementation N4FlickerImageCacheInfo
@end

@implementation N4FlickrImageListViewController
{
    N4FlickerImageSource *_imageSource;

    NSMutableArray *_imageCache;    // TODO: I do not think this is the best idea

    NSOperationQueue *_previewQueue;
    UIActivityIndicatorView *_activityView;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        //localizations not important?
        self.title = @"Recent Photos";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    _activityView = [self createActivityView];
}

//Should be moved into a separate view class i.e. N4FlickrImageListView
- (UIActivityIndicatorView *) createActivityView
{
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityView sizeToFit];
    [activityView setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    return activityView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(openFavouritesAction)];
    
    [self setNavBarRightButtonState:true];
    self.tableView.rowHeight = 75.0f;
    [self.tableView registerClass:[N4FlickrImageCell class]
        forCellReuseIdentifier:NSStringFromClass([N4FlickrImageCell class])];

    _imageCache = [NSMutableArray new];
    _imageSource = [N4FlickerImageSource new];
    _previewQueue = [NSOperationQueue new];
    

    [self updatePhotos];
}

- (void) openFavouritesAction
{

    N4FavouriteListViewController *favListViewCon = [[N4FavouriteListViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:favListViewCon];
    [self presentViewController: nav animated:true completion:nil];
}

- (void) setNavBarRightButtonState:(BOOL) loading {
    self.navigationItem.rightBarButtonItem = nil;
    if(loading) {
        [_activityView startAnimating];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_activityView];
    } else {
        [_activityView stopAnimating];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(updatePhotos)];
    }
}

- (void)updatePhotos
{
    [self setNavBarRightButtonState:true];
    
    [_imageSource fetchRecentImagesWithCompletion:^{
        NSLog(@"completion block reached");
            [_activityView stopAnimating];
            [self.tableView reloadData];
            [self setNavBarRightButtonState:false];
            
    }];
}

#pragma mark - UITableView DataSource/Delegate methods

- (UITableViewCell *)tableView:(UITableView *)tableView
	cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	N4FlickrImageCell *cell = [tableView dequeueReusableCellWithIdentifier:
        NSStringFromClass([N4FlickrImageCell class])];
    N4FlickrImage *flickrImage = [_imageSource imageAtIndex:indexPath.row];
    cell.title = flickrImage.title;

    cell.previewImage = [UIImage imageNamed:@"spinner.gif"];
    [cell setPreviewImageFromUrlString: flickrImage.previewURL];

	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _imageSource.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // show the selected image in our image view controller

	N4FlickrImageViewController *ctrl = [[N4FlickrImageViewController alloc]
                                         initWithFlickrImage:[_imageSource imageAtIndex:indexPath.row]];
    [self.navigationController pushViewController:ctrl animated:YES];
}
@end