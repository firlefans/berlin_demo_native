//
//  QueueHelper.h
//  FlickrTable
//
//  Created by Mark Frawley on 28/09/2013.
//  Copyright (c) 2013 NumberFour AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QueueHelper : NSObject

+ (void) execSync:(void (^)(void))completionBlock;
+ (void) execAsync:(void (^)(void))completionBlock;

@end
